<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;
use TCG\Voyager\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
            $role = Role::where('name', 'admin')->firstOrFail();

            User::create([
                'name'           => 'Admin',
                'email'          => 'admin@admin.com',
                'password'       => bcrypt(config('voyager.adminPassword')),
                'remember_token' => str_random(60),
                'role_id'        => $role->id,
            ]);
            User::create([
                'name'           => 'dat_admin',
                'email'          => 'datdinhxuan1602@gmail.com',
                'password'       => bcrypt('dat_admin'),
                'remember_token' => str_random(60),
                'role_id'        => $role->id,
            ]);
            User::create([
                'name'           => 'huy_admin',
                'email'          => 'huy123s1s1qwe@gmail.com',
                'password'       => bcrypt('huy_admin'),
                'remember_token' => str_random(60),
                'role_id'        => $role->id,
            ]);
        }
    }
}
