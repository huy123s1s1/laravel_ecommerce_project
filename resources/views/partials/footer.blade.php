<footer>
    <div class="footer-content container">
        <div class="made-with">Made with <i class="fa fa-heart heart"></i> by Doan Quoc Huy ft Dinh Xuan Dat</div>
        {{ menu('footer', 'partials.menus.footer') }}
    </div> <!-- end footer-content -->
</footer>
